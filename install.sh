#!/bin/sh

git submodule init
git submodule update

ln -sf `pwd`/zshrc ~/.zshrc
ln -sf `pwd`/oh-my-zsh ~/.oh-my-zsh
ln -sf `pwd`/dircolors/dircolors.256dark ~/.dir_colors
